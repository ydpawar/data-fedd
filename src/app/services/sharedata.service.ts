import {PLATFORM_ID, Injectable, Inject, EventEmitter} from '@angular/core';
import {BehaviorSubject, Subject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedataService {

  public platformId;
  private data = {};
  private Setting = [];

  headerCall: EventEmitter<any> = new EventEmitter();

  routeChange = new BehaviorSubject<any>('');
  itemsRouteChange = this.routeChange.asObservable();

  globleCommantryChange = new BehaviorSubject<any>(''); // GLOBLE COMMANTRY
  itemsglobleCommantryChange = this.globleCommantryChange.asObservable();

  balanceChange = new BehaviorSubject<any>(''); //BALANCE iNFO
  itemsbalanceChangeChange = this.balanceChange.asObservable();

  private subject = new Subject<any>();

  constructor(@Inject(PLATFORM_ID) platformId: Object) {
    this.platformId = platformId;
  }

  setOption(option, value) {
    this.data[option] = value;
  }

  getOption() {
    return this.data;
  }

  getSingleOption(option) {
    return this.data[option];
  }

  setRouterChange(data: number) {
    this.routeChange.next(data);
  }

  setGlobleCommantryChange(data: any) {
    this.globleCommantryChange.next(data);
  }

  setBalanceInfo(data: any) {
    this.balanceChange.next(data);
  }

  sendCall(flag: number) {
    this.subject.next(flag);
  }

  getCall(): Observable<any> {
    return this.subject.asObservable();
  }

  setSettingOption(option, value) {
    this.Setting[option] = value;
  }

  getSettingOption() {
    return this.Setting;
  }

  getSingleSettingOption(option) {
    return this.Setting[option];
  }

}
