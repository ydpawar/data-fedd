import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { tap, retry, shareReplay } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';
import { EnvService } from './env.service';
import { User } from '../share/models/user';


@Injectable({
  providedIn: 'root'
})

export class AuthService {
  isLoggedIn:boolean = false;

  token: any;
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(
    private http: HttpClient,
    private env: EnvService,
    private route: Router
  ) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  login(data) {
    return this.http.post(this.env.API_URL + 'login', data).pipe(
      tap((token: any) => {
        if (token.status === 1) {
          localStorage.setItem('currentUser', JSON.stringify(token.data));
          this.token = token.data;
          this.isLoggedIn = true;
          this.currentUserSubject.next(this.token);
        }
        return token;
      }),
    );
  }

  changePassword(data) {
    return this.http.post(this.env.API_URL + 'change-password', data).pipe(
      tap((token: any) => {
        return token;
      }),
    );
  }

  logout() {

    return this.http.get(this.env.API_URL + 'logout')
      .pipe(
        tap(data => {
          localStorage.clear();
          sessionStorage.clear();
          this.currentUserSubject.next(null);
          this.isLoggedIn = false;
          delete this.token;
          return data;
        })
      );
  }

  accessDenide() {
    localStorage.clear();
    sessionStorage.clear();
    this.currentUserSubject.next(null);
    this.isLoggedIn = false;
    delete this.token;
    this.route.navigate(['/login']);
  }

  headerCall() {
    return this.http.post<any>(this.env.API_URL + 'user-data', {})
      .pipe(
        retry(4),
        tap(user => {
          if ([444, 403].indexOf(user.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            this.accessDenide();
            // location.reload(true);
          }
          return user;
        }, err => {
          if (err.status === 0 && err.code === 444) {
            this.accessDenide();
          }
        }),
        shareReplay()
      );
  }


  getToken() {
    this.token = JSON.parse(localStorage.getItem('currentUser'));
    return JSON.parse(localStorage.getItem('currentUser'));
  }

  accessDenied() {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('event');
    localStorage.clear();
    sessionStorage.clear();
    this.currentUserSubject.next(null);
    this.isLoggedIn = false;
    delete this.token;
    return false;
  }

  register(data) {
    return this.http.post<any>(this.env.API_URL + 'client/enquiry', data)
      .pipe(
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            this.accessDenide();
            // location.reload(true);
          }
          return res;
        })
      );
  }

  getInstrumentList() {
    // console.log('bhb');
    return this.http.get<any>(this.env.API_URL + 'instrument/list')
      .pipe(
        retry(4),
        tap(res => {
          return res;
        }),
        shareReplay()
      );
  }

  requestInstrument(data) {
    return this.http.post<any>(this.env.API_URL + 'request/instrument', data)
      .pipe(
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            this.accessDenide();
            // location.reload(true);
          }
          return res;
        })
      );
  }

  // requestInstrument(data) {
  //   return this.http.get<any>(this.env.API_URL + 'request/instrument', data) // , {headers: headers}
  //     .pipe(
  //       retry(4),
  //       tap(res => {
  //         return res;
  //       }),
  //       shareReplay()
  //     );
  // }
}
