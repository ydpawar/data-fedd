import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndexComponent } from './index.component';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuard } from '../guard/auth.guard';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

const routes: Routes = [
  {
    path: '',
    component: IndexComponent,
    children:[
      {
        path: 'dashboard',
        component: DashboardComponent,
        canActivate :[AuthGuard]
      },
      {path:'',redirectTo:'dashboard',pathMatch:'full'}
    ]
  },

]

@NgModule({
  declarations: [
    IndexComponent ,
    DashboardComponent,
  ],
  imports: [
    CommonModule, RouterModule.forChild(routes),FormsModule,ReactiveFormsModule,HttpClientModule
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class IndexModule { }
