import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

interface listData {
  api_purchase_date: string;
  api_renewal_date: string;
  id: number;
  name: string;
  no_of_element: string;
  status: number;
  total_payment: string;
  uid: number;
}


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [AuthService]
})
export class DashboardComponent implements OnInit, OnDestroy {

  public data: listData[] = [];
  public dataList: listData[] = [];
  isEmpty = false;
  isSubmit = false;

  public form: FormGroup;
  public successMsg: string;
  public errorMsg: string;

  currentUser :any ;

  constructor(
    private service: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,) {
      this.currentUser = localStorage.getItem('currentUser');
      this.createForm();
  }

  ngOnInit(): void {
    
    this.getList();
  }

  ngOnDestroy() {

    location.reload()
  }

  logout(): void {
    this.service.logout().subscribe((res) => {
      localStorage.clear();
      sessionStorage.clear();
      this.router.navigate(['']);
    });
  }

  getList() {
    this.service.getInstrumentList().subscribe(
      (data) => {
        this.onSuccessDataList(data);
      },
      error => {
        console.log(error);
      });
  }

  onSuccessDataList(response) {

    if (response.status !== undefined && response.status === 1) {
      if (response.data !== undefined && response.data !== undefined) {

        this.isEmpty = true;
        this.dataList = response.data;
        // console.log(this.dataList);
      }
    }
    this.isEmpty = false;
  }

  createForm() {
    this.form = this.formBuilder.group({
      instrument: ['', Validators.required],
    });
  }

  submitReq() {
    this.errorMsg = '';
    this.isSubmit = true;
    const data = this.form.value;
    // console.log(data);
    if (this.form.valid) {
      this.service.requestInstrument(data).subscribe(
        res => {
          if (res.code === 444 && res.status === 0) {
            this.errorMsg = res.message;
          }
          else {
            this.successMsg = res.message;
            this.form.reset();
          }
        }
      );
    }
    this.isSubmit = false;
  }

  get frmInstrument() { return this.form.get('instrument'); }

}
