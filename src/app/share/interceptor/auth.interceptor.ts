import {Injectable} from '@angular/core';
import {HttpRequest, HttpHandler, HttpEvent, HttpInterceptor} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AuthService} from '../../services/auth.service';
import * as CryptoJS from 'crypto-js';

@Injectable()
export class BasicAuthInterceptor implements HttpInterceptor {
  enableEncryption: any = false;
  orignalHash: string = '';

  constructor(private authenticationService: AuthService) {
  }

  private generateRandom(length) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  private generateHash() {
    // const time = moment().format();
    const time = new Date().getTime();
    let resData = this.generateRandom(6);
    resData += time;
    this.orignalHash = resData;
    // console.log('signature', resData);
    const secretKeyBytes = '$omemeta$alt';
    const signatureBytes = CryptoJS.HmacSHA256(resData, secretKeyBytes);
    const signatureBase64String = CryptoJS.enc.Base64.stringify(signatureBytes);
    //  console.log('signatureBase64', signatureBase64String);
    return signatureBase64String;

  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add authorization header with basic auth credentials if available
    const currentUser: any = this.authenticationService.currentUser;
    if (currentUser && currentUser.source && currentUser.source._value && currentUser.source._value.token && request.url.indexOf(':8443') === -1) {

      let bodyTmp = request.body;
      if (request.method.toLocaleUpperCase() === 'POST') {
        bodyTmp['tnp'] = this.orignalHash;
      }
      request = request.clone({
        setHeaders: {
          'Authorization': 'Bearer '+currentUser.source._value.token,
          'content-type': `application/json`,
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': 'true',
        //  'encryption': this.enableEncryption,
         'hash': this.generateHash(),
          'platform': 'web',
          'apiversion': '1.0'
        },
      body: bodyTmp

      });

    } else {
      request = request.clone({
        setHeaders: {
          'content-type': `application/json`,
          'platform': 'web',
          'apiversion': '1.0',
         'Access-Control-Allow-Origin': '*',
         'Access-Control-Allow-Credentials': 'true'
        }
      });
    }

    return next.handle(request);
  }
}
