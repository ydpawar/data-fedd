import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ThemeComponent } from './theme.component';
import { ContactComponent } from './contact/contact.component';
import { AboutComponent } from './about/about.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

const routes: Routes = [
  {
    path: '',
    component: ThemeComponent,
  },
  {
    path: 'contact',
    component: ContactComponent
  },
  {
    path: 'about',
    component: AboutComponent
  },
  {
    path: '**',
    redirectTo: '404',
    pathMatch: 'full'
  }
]

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes), 
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [ThemeComponent,
  ]
})
export class ThemeModule { }
