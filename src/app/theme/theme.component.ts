import { Component, OnInit, Renderer2, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { SharedataService } from '../services/sharedata.service';

declare var $

@Component({
  selector: 'app-theme',
  templateUrl: './theme.component.html',
  styleUrls: ['./theme.component.css'],
  providers: [AuthService]
})
export class ThemeComponent implements OnInit, OnDestroy {

  public form: FormGroup;
  public successMsg: string;
  public errorMsg: string;
  public systemInActiveMsg: string;
  public passwordType: string = 'password';
  public firstScreen: boolean = false;
  public isSubmit: boolean = false;
  public isActiveSYS: number = 0;

  public formRgit: FormGroup;

  constructor(
    private renderer: Renderer2,
    public _router: Router,
    public router: ActivatedRoute,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private share: SharedataService,
  ) {
    this.createForm();
    /*******Back button disable***********/
    window.location.hash = 'login';
    window.location.hash = 'Again-No-back-button'; // again because google chrome don't insert first hash into history
    window.onhashchange = function () {
      window.location.hash = 'login';
    };
  }

  ngOnInit(): void {
  }

  ngOnDestroy():void {
    location.reload()
  }

  createForm() {
    this.form = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
      platform: ['web'],
      systemId: ['1'],
    });

    this.formRgit = this.formBuilder.group({
      name: ['', [Validators.required]],
      phone: ['', [Validators.required]],
      email: ['', [Validators.required]],
      address: ['', [Validators.required]],
      instrument: ['', [Validators.required]],
    });
  }

  submitLogin() {
    this.errorMsg = '';
    this.isSubmit = true;
    const data = this.form.value;
    if (this.form.valid && !this.isActiveSYS) {
      this.authService.login(data).subscribe(
        res => {
          if (res.code === 444 && res.status === 0) {
            this.errorMsg = res.message;
          } else {
            if (res.status === 1) {
              this.successMsg = res.message;

              this._router.navigate(['/dashboard']);

            }
          }
        },
        error => {
          console.log(error);
        }
      );
    }
    this.isSubmit = false;
  }

  get frmuUsername() { return this.form.get('username'); }
  get frmPassword() { return this.form.get('password'); }


  logout() {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('event');
  }

  closeMsg() {
    this.errorMsg = '';
  }

  submitRegister() {
    this.errorMsg = '';
    this.isSubmit = true;
    const data = this.formRgit.value;
    if (this.formRgit.valid) {
      this.authService.register(data).subscribe(
        res => {
          if (res.code === 444 && res.status === 0) {
            this.errorMsg = res.message;
          }
          else {
            this.successMsg = res.message;
            this.formRgit.reset();
          }
        }
      );
    }
  }

  get frmcname() { return this.formRgit.get('name'); }
  get frmphone() { return this.formRgit.get('phone'); }
  get frmemail() { return this.formRgit.get('email'); }
  get frmaddress() { return this.formRgit.get('address'); }
  get frminstrument() { return this.formRgit.get('instrument'); }

}
