export const environment = {
  production: true,
  version: '1.0.2',
  API_URL: 'http://52.66.104.39/ApiserviceClient/public/api/',
  API_LIVE_URL: 'https://metagoldtrader.com:8443/dev/',
};
